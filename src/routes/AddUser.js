const express = require("express");
const router = express.Router();
require("../db/conn"); /*  DB Connection file import  */
const userRegisterModel = require("../model/UserRegisterModel");

router.get("/", (req, res) => {
	res.send("Welcome Ganesh Dubey (Pass Through Route)");
});
router.post("/AddUser", (req, res) => {
	const { name, mobileNumber, email, password } = req.body;
	if (name && mobileNumber && email && password) {
		userRegisterModel
			.findOne({ email: email })
			.then((userExist) => {
				if (userExist) {
					return res.status(404).send({
						status: 404,
						message: "Allready Registered User ",
						result: userExist
					});
				}
				const NewRegister = new userRegisterModel({
					name,
					mobileNumber,
					email,
					password,
					photo:''
				});
				NewRegister.save()
					.then(() => {
						userRegisterModel.find((err, docs) => {
							if (!err) {
								res.status(200).send({
									status: 200,
									message: "User Added Success",
									result: docs
								});
							} else {
								console.log("Failed to retrieve the Course List: " + err);
							}
						});
					})
					.catch((err) => {
						console.log(err);
					});
			})
			.catch((err) => {
				console.log(err);
			});
	} else {
		res.status(404).send({
			status: 404,
			message: "Check All Fields"
		});
	}
});
// UserLogin
router.post("/UserLogin", (req, res) => {
	const { email, password } = req.body;
	if (email && password) {
		userRegisterModel.findOne({ email: email }).then((userExist) => {
			if (userExist) {
				let DbPassword=userExist.password
                if(DbPassword==password){
                    res.status(200).send({
                        status: 200,
                        message: "User Found & Login Success",
                        result:userExist
                    });
                }
                else{
                    res.status(404).send({
                        status: 404,
                        message: "Password In-Correct"
                    });
                }
			} else {
				res.status(404).send({
					status: 404,
					message: "User Not Found & Login Failed "
				});
			}
		});
	} else {
		res.status(404).send({
			status: 404,
			message: "Check All Fields"
		});
	}
});
module.exports = router;

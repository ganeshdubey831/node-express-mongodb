const express = require("express");
const router = express.Router();
const multer  = require('multer')
const fs=require('fs')
require("../db/conn"); /*  DB Connection file import  */
const homeGalleryModel = require("../model/HomeGalleryModel");
const { captureRejectionSymbol } = require("events");

var storage=multer.diskStorage({
	destination:(req,file,cb)=>{
		cb(null,'./backendUpload/GalleryImages')
	},
	filename:(req,file,cb)=>{
     cb(null,Date.now()+file.originalname)
	}
})

const checkFileType=(file,cb)=>{
  if(file.mimetype==='image/jpeg'||file.mimetype==='image/jpg'||file.mimetype==='image/png'){
	cb(null,true);
  }
  else{
	cb(null,false)
  }
}

var upload=multer({
	storage:storage,
	fileFilter:(req,file,cb)=>{
		checkFileType(file,cb)
	}
})
router.post("/AddHomeGallery",upload.single('galleryImg'),(req, res) => {
  if(req.file){
		const {title,description}=req.body;
		const pathName=req.file.path;
    const NewHomeGallery = new homeGalleryModel({
      title,
      description,
      image:`http://localhost:4000/${pathName}`
    });
    NewHomeGallery.save()
					.then(() => {
						res.status(200).send({
							status: 200,
							message: "HomeGallery Added Success"
						});
					})
					.catch((err) => {
						console.log(err);
					});
  }
})
router.get("/GetHomeGallery",(req, res) => {
  homeGalleryModel.find((err, docs) => {
		if (docs) {
			res.status(200).send({
				status: 200,
				message: "HomeGallery List Success",
				result: docs
			});
		} else {
			console.log("Failed to retrieve the HomeGallery List : " + err);
		}
	});
})
router.post("/DeleteHomeGallery",(req, res) => {
	const{id,path}=req.body;
	homeGalleryModel.findOne({_id:id},(err, docs) => {
		if (docs) {
			docs.remove()
			res.status(200).send({
				status: 200,
				message: "HomeGallery Delete Sucessfull",
			});
		} 
		else if(!docs){
			res.status(400).send({
				status: 400,
				message: "Invalid Id"
			});
		}
	});
  })
  router.post("/UpdateHomeGallery",(req, res) => {
	const {id,changes}=req.body;
    homeGalleryModel.updateOne({_id:id},{$set:{...changes}},(err,docs)=>{
        if (docs) {
			if(docs.modifiedCount==0){
				res.status(400).send({
					status: 400,
					message: docs.modifiedCount==0?"Nothing Changes ":"Update Success"
				});
			}
			else{
				res.status(200).send({
					status: 200,
					message: docs.modifiedCount==0?"Nothing Changes ":"Update Success"
				});
			}
            		
        }
        else{
            res.status(404).send({
                status: 404,
                message: "err",
            }); 
        }
    });
  })
  router.post("/UpdateHomeGalleryImg",upload.single('galleryImg'),(req, res) => {
	const {id,title,description}=req.body;
	if(req.file){
		const pathName=req.file.path;
		homeGalleryModel.updateOne({_id:id},{$set:{
			image:`http://localhost:4000/${pathName}`,
			title,
			description
		}},(err,docs)=>{
			if (docs) {
				if(docs.modifiedCount==0){
					res.status(400).send({
						status: 400,
						message: docs.modifiedCount==0?"Nothing Changes ":"Update Success"
					});
				}
				else{
					res.status(200).send({
						status: 200,
						message: docs.modifiedCount==0?"Nothing Changes ":"Update Success"
					});
				}
						
			}
			else{
				res.status(404).send({
					status: 404,
					message: "err",
				}); 
			}
		});
	}
  })
module.exports = router;

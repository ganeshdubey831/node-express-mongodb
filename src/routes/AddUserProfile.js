const { response } = require("express");
const express = require("express");
const router = express.Router();
const multer  = require('multer')


require("../db/conn"); /*  DB Connection file import  */
const userProfileModel = require("../model/UserProfileModel"); /* UserProfile Model import  */
const userRegisterModel=require("../model/UserRegisterModel") /* UserRegister Model import  */


var storage=multer.diskStorage({
	destination:(req,file,cb)=>{
		cb(null,'./backendUpload/UserImg')
	},
	filename:(req,file,cb)=>{
     cb(null,Date.now()+file.originalname)
	}
})

const checkFileType=(file,cb)=>{
  if(file.mimetype==='image/jpeg'||file.mimetype==='image/jpg'||file.mimetype==='image/png'){
	cb(null,true);
  }
  else{
	cb(null,false)
  }
}

var upload=multer({
	storage:storage,
	fileFilter:(req,file,cb)=>{
		checkFileType(file,cb)
	}
})

router.post('/uploadUserImg',upload.single('myImg'),async(req,res,next)=>{
	if(req.file){
		const {id}=req.body;
		const pathName=req.file.path;
		// res.status(200).send({
		// 	file:req.file,
		// 	pathName
		// })
		userRegisterModel.updateOne({_id:id},{$set:{photo:`http://localhost:4000/${pathName}`}},(err,docs)=>{
			if (docs) {
				if(docs.modifiedCount==0){
					res.status(400).send({
						status: 400,
						message: docs.modifiedCount==0?"Nothing Changes ":"Update Success"
					});
				}
				else{
					res.status(200).send({
						status: 200,
						message: docs.modifiedCount==0?"Nothing Changes ":"Update Success"
					});
				}
						
			}
			else{
				res.status(404).send({
					status: 404,
					message: "err",
				}); 
			}
		});
	}
})
// AddUserProfile
router.post("/AddUserProfile", (req, res) => {
	const { id, gender, address, city, state, pincode } = req.body;
	if (id && gender && address && city && state && pincode) {
		userProfileModel.findOne({ id: id }, (err, result) => {
			if (err) {
				res.status(400).send({
					status: 400,
					message: "User Invalid"
				});
			} else if (!result) {
				const UserProfile = new userProfileModel({
					id,
					gender,
					address,
					city,
					state,
					pincode
				});
				UserProfile.save()
					.then(() => {
						res.status(200).send({
							status: 200,
							message: "User Profile Added Success"
						});
					})
					.catch((err) => {
						console.log(err);
					});
			} else if (result) {
				res.status(400).send({
					status: 400,
					message: "User Profile Allready Added "
				});
			}
		});
	} else {
		res.status(404).send({
			status: 404,
			message: "Check All Fields"
		});
	}
});

// GetUserProfile
router.post("/GetUserProfile", (req, res) => {
	const { id } = req.body;
	if (id) {
		userProfileModel.findOne({ id: id }, (err, res1) => {
			if (err) {
				res.status(400).send({
					status: 400,
					message: "Internal Server Error"
				});
			}
			else if (!res1) {
				res.status(400).send({
					status: 400,
					message: "Invalid User Id OR No Profile Added By This User"
				});
			} else {
                userRegisterModel.findOne({_id:id},(err,res2)=>{
                    if(res2){
                        let mergeData={...res1._doc,...res2._doc}
                        res.status(200).send({
                            status: 200,
                            message: "Success",
                            result:mergeData
                        });

                    }
                })

			}
		});
	} else {
		res.status(404).send({
			status: 404,
			message: "Check All Fields"
		});
	}
});

// GetAllUserProfile
router.get("/GetAllUserProfile", (req, res) => {
	userProfileModel.find((err, docs) => {
		if (docs) {
			res.status(200).send({
				status: 200,
				message: "Get All User List Success",
				result: docs
			});
		} else {
			console.log("Failed to retrieve the All User List: " + err);
		}
	});
});
// GetAllUserRegister
router.get("/GetAllUserRegister", (req, res) => {
	userRegisterModel.find((err, docs) => {
		if (docs) {
			res.status(200).send({
				status: 200,
				message: "Get All User List Success",
				result: docs
			});
		} else {
			console.log("Failed to retrieve the All User List: " + err);
		}
	});
});


// UpdateUserProfileById
router.post("/UpdateUserProfile", (req, res) => {
    const {id,changes}=req.body;
    userProfileModel.updateOne({id:id},{$set:{...changes}},(err,docs)=>{
        if (docs) {
            		res.status(200).send({
            			status: 200,
            			message: docs.modifiedCount==0?"Nothing Changes In Profile":"User Profile Update Success"
            		});
        }
        else{
            res.status(404).send({
                status: 404,
                message: "err",
            }); 
        }
    });
});
// UpdateRegisterUserById
router.post("/UpdateRegisterUser",upload.single('myImg'), (req, res) => {
    const {id,changes}=req.body;
    userRegisterModel.updateOne({_id:id},{$set:{...changes}},(err,docs)=>{
        if (docs) {
			if(docs.modifiedCount==0){
				res.status(400).send({
					status: 400,
					message: docs.modifiedCount==0?"Nothing Changes ":"Update Success"
				});
			}
			else{
				res.status(200).send({
					status: 200,
					message: docs.modifiedCount==0?"Nothing Changes ":"Update Success"
				});
			}
            		
        }
        else{
            res.status(404).send({
                status: 404,
                message: "err",
            }); 
        }
    });
});
// GetAllUserProfile
router.post("/DeleteUserProfile", (req, res) => {
	const{id}=req.body;
	userProfileModel.findOne({id:id},(err, docs) => {
		if (docs) {
			docs.remove()
			res.status(200).send({
				status: 200,
				message: "User Profile Delete Sucessfull",
			});
		} 
		else if(!docs){
			res.status(400).send({
				status: 400,
				message: "Invalid User Id"
			});
		}
	});
});
// DeleteRegisterUser
router.post("/DeleteRegisterUser", (req, res) => {
	const{id}=req.body;
	userRegisterModel.findOne({_id:id},(err, docs) => {
		if (docs) {
			docs.remove()
			res.status(200).send({
				status: 200,
				message: "User Delete Sucessfull",
			});
		} 
		else if(!docs){
			res.status(400).send({
				status: 400,
				message: "Invalid User Id"
			});
		}
	});
});

module.exports = router;

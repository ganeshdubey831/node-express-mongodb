const mongoose = require("mongoose");
const UserProfileSchema = new mongoose.Schema({
    id: { type: mongoose.Schema.Types.ObjectId, required: true },
    gender: { type: String, required: true },
	address: { type: String, required: true },
	city: { type: String, required: true },
    state: { type: String, required: true },
    pincode: { type: String, required: true },
});
const UserProfileM = mongoose.model("USERPROFILE", UserProfileSchema);
module.exports=UserProfileM;
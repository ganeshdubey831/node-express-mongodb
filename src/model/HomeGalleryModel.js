const mongoose = require("mongoose");
const HomeGallerySchema = new mongoose.Schema({
    image: { type: String, required: true },
	title: { type: String, required: true },
	description: { type: String, required: true },
});
const HomeGalleryM = mongoose.model("HOMEGALLERY", HomeGallerySchema);
module.exports=HomeGalleryM;
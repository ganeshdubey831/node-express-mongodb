
const mongoose = require("mongoose");
const express = require("express");
var cors = require('cors')
const app = express();

app.use(cors())
app.use(express.static(__dirname + '/'));  /* Get Local Uploaded Img*/ 

app.use(express.json())  
const dotenv = require("dotenv");
let port = process.env.PORT||4000;
dotenv.config();



require('./src/db/conn')   /*  DB Connection file import  */
app.use(require('./src/routes/AddUser'))
app.use(require('./src/routes/AddUserProfile'))
app.use(require('./src/routes/ForgetPassword'))
app.use(require('./src/routes/HomeGallery'))



const middelware=(req,res,next)=>{
    console.log("hello this is my middelware")
    next();
    }
// let hostname="192.168.43.56"
app.get("/about",middelware, (req, res) => {
	res.send("Welcome About Page");
});

app.listen(port,() => {
	console.log(`server up and running on port ${port}`);
    // console.log(process.env.DB_CONNECT)
});